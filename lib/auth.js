/* @flow */

'use strict';

var R = require('../vendor/ramda'),
    Request = require('kakuna'),
    endpoint = require('./url'),
    event = require('./event'),
    attendee = require('./attendee');

var login = R.curry(function (user, password) {
  return Request.get(endpoint('tokens')).set('Accept', 'application/json').auth(user, password).end().then(getLoggedApi, Oops);
});

var genApi = function genApi(token) {
  return {
    token: token
  };
};

var getLoggedApi = function getLoggedApi(response) {
  return R.compose(attendee, event, genApi, R.path(['body', 'token']), checkForErrors)(response);
};

var checkForErrors = function checkForErrors(res) {
  return res.ok ? res : Oops();
};

var Oops = function Oops() {
  throw new Error('Not able to login.');
};

module.exports = {
  login: login
};