var auth       = require('../lib/auth')
  , expect     = require('chai').expect
  , R          = require('ramda')

describe('Attendees', () => {
  var event = 15370
  var attendees = [{
      id: 577037
    , name: "Ju Goncalves"
    , email: "code@jugoncalv.es"
    , code: "HVLOXJKG"
    , ticket_type: "ticket"
    , checked_at: null
  }]
  var attendee = {
      id:577037
    , name: "Ju Goncalves"
    , email: "code@jugoncalv.es"
    , code: "HVLOXJKG"
    , ticket_type: "ticket"
    , checked_at: null
    , answers: []
  }
  var valid = {
      user: 'code@jugoncalv.es'
    , password: '123123'
  }
  
  var deepEquality = R.curry((value, result) => {
    expect(result).to.be.deep.equal(value)
  })
  
  var getAttendees = (api) => {
    return api.getList(event)
  }
  
  var getAttendee = (api) => {
    return api.getList(event, attendee.id)
  }
  
  var api = auth.login(valid.user, valid.password).then(R.prop('attendee'))
  
  it('should get all attendees', () => {
    api
      .then(getAttendees)
      .then(deepEquality(attendees))
  })
  
  it('should get one attendee', () => {
     api
      .then(getAttendee)
      .then(deepEquality(attendee))
  })
})
