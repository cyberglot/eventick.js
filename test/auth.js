var auth       = require('../lib/auth')
  , expect     = require('chai').expect
        
describe('Authentication', () => {
  var valid = {
    user: 'code@jugoncalv.es',
    password: '123123'
  }
  
  var invalid = {
    user: 'hello@jugoncalv.es',
    password: '123123'
  }
  
  it('should log in', () => {
    auth.login(valid.user, valid.password)
      .then((api) => {
        expect(api).to.include.keys('token', 'event', 'attendee')
        expect(api.token).to.be.ok
      })
    
  })
  
  it('should not log in', () => {
    auth.login(invalid.user, invalid.password)
      .catch((msg) => {
        expect(msg).to.be.equal('Not able to login.')
      })
  })
})
