var auth       = require('../lib/auth')
  , expect     = require('chai').expect
  , R          = require('ramda')
        
describe('Events', () => {
  var events = [{ 
      id:15370
    , title:"Teste evento 123123"
    , start_at:"2045-05-24 16:00:00 -0300"
  }]
  var event = {
      id: 15370
    , title: "Teste evento 123123"
    , venue: null
    , slug: "teste-evento-123123"
    , start_at: "2045-05-24 16:00:00 -0300"
    , links: {
      tickets: [{ id: 34176, name: "ticket"}]
    }}
  var valid = {
      user: 'code@jugoncalv.es'
    , password: '123123'
  }
  
  var deepEquality = R.curry((value, result) => {
    expect(result).to.be.deep.equal(value)
  })
  
  var getEvents = (api) => {
    return api.getList()
  }
  
  var getEvent = (api) => {
    return api.getList(event.id)
  }
  
  var api = auth.login(valid.user, valid.password).then(R.prop('event'))
  
  it('should get all events', () => {
    api
      .then(getEvents)
      .then(deepEquality(events))
  })
  
  it('should get one event', () => {
     api
      .then(getEvent)
      .then(deepEquality(event))
  })
})
