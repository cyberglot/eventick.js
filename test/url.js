var url       = require('../lib/url')
  , expect    = require('chai').expect
        
describe('Build api endpoints', () => {
  it('(url) path => https://www.eventick.com.br/api/v1/path.json', () => {
    expect(url('path')).to.be.equal('https://www.eventick.com.br/api/v1/path.json')
  })
})
